# HiAsm96
HiAsm on Windows 96!

## Features
* Written in multiple languages
* Supports Custom HiAsm packs
* All features of HiAsm 4
* Can compile the `.hws` scripts used in HiAsm packs

## Open-Source Licenses
* **SQL.JS** https://github.com/sql-js/sql.js/

## Languages Supported
* English (English)
* Spanish (Español)
* French (Français)
* German (Deutsch)
* Russian (Русский)
